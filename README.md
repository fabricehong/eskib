# Readme
## 1. Mount Elasticsearch container
in Docker directory, type:
```
docker-compose up
```

start kibana on : http://localhost:5601

upload to elasticsearch on : http://localhost:9200/

## 2. Prepare python environment
Preferrably create an environment with virtualenv / virtualenvwrapper

In the environment, get the required dependencies:
```
pip install -r python/requirements.txt
```

## 3. Run the Twitter example
This exemple download the twitter feeds of the redaction chiefs of
the journal "Le Temps" and upload the data on elasticsearch.

### 3.1 Configure access keys for using Twitter API
Check the twitter api documentation to configure the file *tweepy_get*.

Set variables:
* consumer_key
* consumer_secret
* access_key
* access_secret
### 3.1 Download the feeds to csv
run script *download_feed.py*

### 3.2 Upload data to elasticsearch
run script *es_import_tweets.py*.
It will import the CSV files located in
```
/csvs/le_temps/tweets
```

## 4. Visualize data in Kibana
### 4.1 Select index
Go to : http://localhost:5601

The index created (configured in *es_import_tweets.py*) is named *tweets*

In Kibana, enter *tweets* under *Index name or pattern*, select the time based
field and click *create*

### 4.2 Browse data
Go to Discover tab

### 4.3 Create visualizations
Go to Visualize tab