from collections import Counter
from . import determine_user_tweet_file

__author__ = 'fabrice'

from urlparse import urlparse
import csv
import re
import dateutil.parser as dateparser

website_pattern = re.compile('\bhttp://')

reference_pattern = re.compile("\\b@([^\s\$]*)")


def read_rows(filename):
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        reader.next()  # skip header
        for row in reader:
            date = dateparser.parse(row[1])
            yield {
                'id': row[0],
                'timestamp': date,
                'text': row[2],
            }


def get_hostnames(text):
    return [urlparse(url).hostname for url in re.findall('\\bhttps?://[^\\b\s]*', text)]


def get_references(text):
    return [t[1:].lower() for t in re.split('[^\w@]', text) if t.startswith('@')]


def get_hashtags(text):
    return [t[1:].lower() for t in re.split('[^\w#]', text) if t.startswith('#')]


def add_to_counter(counter, lst):
    for l in lst:
        counter[l] += 1


def extract_twitter_data(username, statuses):
    hashtags = Counter()
    users = Counter()
    hosts = Counter()
    for status in statuses:
        text = status['text']
        status_references = get_references(text)
        add_to_counter(users, status_references)
        status_hashtags = get_hashtags(text)
        add_to_counter(hashtags, status_hashtags)
        hostnames = get_hostnames(text)
        add_to_counter(hosts, hostnames)
        yield {
            'timestamp': status['timestamp'],
            'username': username,
            'id': status['id'],
            'text': text,
            'hashtags': status_hashtags,
            'refs': status_references,
            'hostnames': hostnames,
        }


def write_csv(filename):
    with open(filename, 'w') as csvfile:
        fieldnames = ['first_name', 'last_name']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})
        writer.writerow({'first_name': 'Lovely', 'last_name': 'Spam'})
        writer.writerow({'first_name': 'Wonderful', 'last_name': 'Spam'})


def extract_twitter_data_for_username(project_name, username):
    filename = determine_user_tweet_file(project_name, username)
    try:
        data = extract_twitter_data(username, read_rows(filename))
    except IOError:
        print "error %s" % filename
        return ()
    return data


def print_test():
    for status in extract_twitter_data(read_rows('gaelhurlimann_tweets.csv')):
        hashtags = status['hashtags']
        if len(hashtags) > 1:
            print('hashtags: ' + str(hashtags) + "\n---")
        refs = status['refs']
        if len(refs) > 1:
            print('refs: ' + str(refs) + "\n---")
        hostnames = status['hostnames']
        if len(hostnames) > 1:
            print('hostnames: ' + str(hostnames) + "\n---")
