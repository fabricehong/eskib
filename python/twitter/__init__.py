import os

__author__ = 'fabrice'

GAEL = 'gaelhurlimann'
JEAN = 'jeanabbiateci'
FRAMMERY = 'cframmery'
JULIE = 'julieconti'
OLIVIER = 'olvierperrin'
AURELIE = 'aureliesf'
FLORIAN = 'floriandel'
CEDRIC = 'cedricgarrofe'
BENOIT = 'sbenoitgodet'

usernames = [
	GAEL,
	JEAN,
	FRAMMERY,
	# OLIVIER, -> doesn't exists anymore
	# AURELIE, -> doesn't exists anymore
	FLORIAN,
	CEDRIC,
	BENOIT
]

def transf(key, value):
	if key == 'hostnames':
		return [hostname.decode('utf8') for hostname in value]
	if key == 'text':
		return value.decode('utf8')
	return value


def transform_obj(obj):
	return {
		key: transf(key, value)
		for key, value in obj.items()
	}


# @gaelhurlimann
# @jeanabbiateci
# @cframmery
# @julieconti
# @olvierperrin -> n'existe plus
# @aureliesf
# @floriandel
# @cedricgarrofe

def get_project_root():
	return os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')


def determine_user_tweet_dir(project_name):
	return os.path.join(get_project_root(), 'csvs', project_name, 'tweets')


def determine_user_tweet_file(project_name, user):
	return os.path.join(determine_user_tweet_dir(project_name), user + '_tweets.csv')


def get_csv_dir(project_name):
	return os.path.join(get_project_root(), 'csvs', project_name)


def determine_csv_output(project_name, name):
	return os.path.join(get_csv_dir(project_name), name + '.csv')
