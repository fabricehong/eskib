from itertools import chain

from python.elasticsearch.elasticsearch_upload import Index

__author__ = 'fabrice'

from python.twitter import tweet_data_extractor, GAEL, transform_obj, usernames
from python.elasticsearch import elasticsearch_upload



tweets_index = Index(
    'tweets',
    [
        ("hashtags", {'raw'}),
        ("refs", {'raw'}),
        ("hostnames", {'raw'}),
        ("username", {'raw'}),
    ],
    transform_obj
)


def index_tweets():
    # data = read_csv.read_data(GAEL)

    all_data = chain(
        *[tweet_data_extractor.extract_twitter_data_for_username('le_temps', username) for username in usernames])

    # elasticsearch_upload.index_data('jenkins.liip.ch', 3333, all_data, only_print=False)
    elasticsearch_upload.index_data('localhost', 9200, all_data, index_descriptions=tweets_index, only_print=False)


index_tweets()
