from collections import defaultdict, Counter

__author__ = 'fabrice'

import csv

__author__ = 'fabrice'

from python.twitter import tweet_data_extractor, usernames, determine_csv_output


class PersonsNodesCsv:
    columns = ['Id', 'Label', 'Category']

    def process_datas(self, datas):
        return set([data['username'] for data in datas])

    def write_data(self, data, writer, username):
        writer.writerow(
            {
                'Id': data,
                'Label': data,
                'Category': 'Tweeter',
            }
        )


class HashtagNodesCsv:
    columns = ['Id', 'Label', 'Category']

    def process_datas(self, datas):
        return set([hashtag for data in datas for hashtag in data['hashtags'] if hashtag])

    def write_data(self, data, writer, username):
        writer.writerow(
            {
                'Id': data,
                'Label': data,
                'Category': 'Hashtag',
            }
        )


class PersonsToPersonsEdgesCsv:
    columns = ['Source', 'Target', 'Id', 'Weight', 'Category']

    def process_datas(self, datas):
        sources = defaultdict(Counter)
        for data in datas:
            for ref in data['refs']:
                if ref in usernames:
                    sources[data['username']][ref] += 1
        return sources.items()

    def write_data(self, data, writer, username):
        source, references = data
        for reference, count in references.items():
            writer.writerow(
                {
                    'Source': source,
                    'Target': reference,
                    'Id': '%s-%s' % (source, reference),
                    'Weight': count,
                    'Category': 'person-to-person'
                }
            )


class PersonsToHashtagsEdgesCsv:
    columns = ['Source', 'Target', 'Id', 'Weight', 'Category']

    def process_datas(self, datas):
        sources = defaultdict(Counter)
        for data in datas:
            for hashtag in data['hashtags']:
                if not hashtag:
                    continue
                sources[data['username']][hashtag] += 1
        return sources.items()

    def write_data(self, data, writer, username):
        person, references = data
        for hashtag, count in references.items():
            writer.writerow(
                {
                    'Source': person,
                    'Target': hashtag,
                    'Id': '%s-%s' % (person, hashtag),
                    'Weight': count,
                    'Category': 'person-to-hashtag'
                }
            )


def write_references(writer, username, csv_file):
    for data in csv_file.process_datas(tweet_data_extractor.extract_twitter_data_for_username('le_temps', username)):
        csv_file.write_data(data, writer, username)


def write_usernames_data_in_file(filename, usernames, csv_file):
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_file.columns)

        writer.writeheader()
        for username in usernames:
            write_references(writer, username, csv_file)


write_usernames_data_in_file(determine_csv_output('le_temps', 'personsNodes'), usernames, PersonsNodesCsv())
write_usernames_data_in_file(determine_csv_output('le_temps', 'hashtagsNodes'), usernames, HashtagNodesCsv())
write_usernames_data_in_file(determine_csv_output('le_temps', 'personsToPersonsEdges'), usernames,
                             PersonsToPersonsEdgesCsv())
write_usernames_data_in_file(determine_csv_output('le_temps', 'personsToHashtagsEdges'), usernames,
                             PersonsToHashtagsEdgesCsv())
