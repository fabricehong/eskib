import csv

from python.elasticsearch import elasticsearch_upload
from python.elasticsearch.elasticsearch_upload import Index

from python.twitter import determine_csv_output, transform_obj

__author__ = 'fabrice'

import dateutil.parser as dateparser

hashtag_reference_index = Index(
    'hashtag_reference',
    [
        ("origin", {'raw'}),
        ("reference", {'raw'}),
        ("hashtag", {'raw'}),
    ],
    transform_obj
)


def create_es_data(filename):
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        reader.next()  # skip header
        for row in reader:
            date = dateparser.parse(row[0])
            yield {
                'timestamp': date,
                'origin': row[1],
                'reference': row[2],
                'hashtag': row[3],
            }


def index_hashtag_references(filename):
    data = create_es_data(filename)
    elasticsearch_upload.index_data('localhost', 9200, data, index_descriptions=hashtag_reference_index,
                                    only_print=False)


index_hashtag_references(determine_csv_output('le_temps', 'references'))
