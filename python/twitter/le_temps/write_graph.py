import csv

__author__ = 'fabrice'

from python.twitter import tweet_data_extractor, usernames, determine_csv_output


class HashtagsCsvFile:
    columns = ['time', 'origin', 'destination', 'hashtag']

    def write_data(self, data, writer, username):
        for reference in data['refs']:
            if reference not in usernames:
                continue
            for hashtag in data['hashtags']:
                writer.writerow(
                    {
                        'time': data['timestamp'],
                        'origin': username,
                        'destination': reference,
                        'hashtag': hashtag,
                    }
                )


def write_references(writer, username, csv_file):
    for data in tweet_data_extractor.extract_twitter_data_for_username('le_temps', username):
        csv_file.write_data(data, writer, username)


def write_in_file(filename, usernames, csv_file):
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_file.columns)

        writer.writeheader()
        for username in usernames:
            write_references(writer, username, csv_file)


write_in_file(determine_csv_output('le_temps', 'references'), usernames, HashtagsCsvFile())
