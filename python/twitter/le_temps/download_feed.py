from python.twitter import determine_user_tweet_file, GAEL, tweepy_get, usernames

__author__ = 'fabrice'

csv_filename = 'gaelhurlimann_tweets.csv'

# @gaelhurlimann
# @jeanabbiateci
# @cframmery
# @julieconti
# @olvierperrin -> n'existe plus
# @aureliesf
# @floriandel
# @cedricgarrofe


for username in usernames:
    print("Downloading feeds for %s" % username)
    tweepy_get.get_all_tweets(username, determine_user_tweet_file('le_temps', username))

