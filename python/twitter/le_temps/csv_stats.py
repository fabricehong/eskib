from collections import Counter
from pprint import pprint
from python import determine_user_tweet_file, GAEL, BENOIT

__author__ = 'fabrice'


from python.twitter import tweet_data_extractor

c = Counter()

hashtag_counter = Counter()
reference_counter = Counter()
number = 0
for data in tweet_data_extractor.extract_twitter_data_for_username('le_temps', BENOIT):
	number += 1
	c['chars'] += len(data['text'])
	c['hashtags'] += len(data['hashtags'])
	c['references'] += len(data['refs'])
	c['http_refs'] += len(data['hostnames'])
	for hashtag in data['hashtags']:
		hashtag_counter[hashtag] +=1
	for reference in data['refs']:
		reference_counter[reference] +=1

print("print number of tweets:")
print(number)
print("tweets global stats:")
pprint(c)
print("hashtags counter:")
pprint(dict(hashtag_counter.most_common(15)))
print("reference counter:")
pprint(dict(reference_counter.most_common(15)))