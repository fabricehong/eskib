import os

__author__ = 'fabrice'

def create_dir_for_file(filename):
	file_dir = os.path.dirname(filename)
	if not os.path.exists(file_dir):
		os.makedirs(file_dir)

def path_relative_to_file(file, relative_path):
	return os.path.join(os.path.dirname(os.path.realpath(file)), relative_path)

class Directory:
	def __init__(self, root_dir):
		self.root_dir = root_dir

	def get_or_create_path(self, *path):
		path = os.path.join(self.root_dir, *path)
		os.makedirs(path, exist_ok=True)
		return path


class Project(Directory):

	@property
	def output(self):
		return OutputDir(self.get_or_create_path('output'))


class OutputDir(Directory):

	def experiment(self, name):
		return Experiment(self.get_or_create_path(name))


class Experiment(Directory):

	def get_csv_dir(self):
		return self.get_or_create_path('csv')

	def csv_file_name(self, name):
		return os.path.join(self.get_csv_dir(), '%s.csv' % name)