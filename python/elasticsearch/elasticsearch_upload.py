from pprint import pprint

from elasticsearch import helpers

__author__ = 'fabrice'

from elasticsearch import Elasticsearch


class Index:
    def __init__(self, name, columns, transform_function = None):
        self._columns = [col[0] for col in columns]
        self._columns_attrs = {
            col_descr[0]: col_descr[1:]
            for col_descr in columns
        }
        self.name = name
        self.transform_function = transform_function if transform_function is not None else lambda x : x

    @property
    def index_name(self):
        return self.name

    @property
    def document_type(self):
        return self.name

    @property
    def column_names(self):
        return self._columns

    def is_col_string(self, name):
        return self._column_has_attribute(name, 'raw')

    def _column_has_attribute(self, name, attribute):
        if not name in self._columns_attrs:
            return False
        attrs = self._columns_attrs[name]
        return attrs and attribute in attrs[0]

    def is_col_number(self, name):
        return self._column_has_attribute(name, 'number')

    def get_string_fields(self):
        return [field for field in self.column_names if self.is_col_string(field)]


def to_bulk_action(index, doc_type, objs, transform_function):
    number_exported = 0
    for obj in objs:
        try:
            source = transform_function(obj)
            yield {
                '_op_type': 'index',
                '_index': index,
                # '_id': document.meta.id,
                '_type': doc_type,
                '_source': source
            }
            number_exported += 1
        except Exception as e:
            print(e)
            continue
    print("exported : %s" % number_exported)


def create_index(es_client, index_description, only_print=False):
    fields = {}

    for field in index_description.column_names:
        if index_description.is_col_string(field):
            fields[field] = {
                'type': 'string',
                'fields': {
                    'raw': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            }
        elif index_description.is_col_number(field):
            fields[field] = {
                'type': 'long',
            }

    create_instruction = {
        "index": index_description.index_name,
        "body": {
            "settings": {
                "number_of_shards": 1
            },
            "mappings": {
                index_description.document_type: {
                    "properties": fields
                }
            }
        }
    }
    if only_print:
        pprint(create_instruction)
    else:
        es_client.indices.create(**create_instruction)


def print_bulk(actions):
    for b in actions:
        pprint(b)


def index_data(es_host, es_port, data, index_descriptions, only_print=False):
    es_client = Elasticsearch(
        host=es_host,
        port=es_port
    )

    def index(index_description):
        index_name = index_description.index_name
        bulk = to_bulk_action(index_name, index_description.document_type, data, index_description.transform_function)
        if not only_print and es_client.indices.exists(index_name):
            es_client.indices.delete(index_name)

        create_index(es_client, index_description, only_print)

        if only_print:
            print_bulk(bulk)
        else:
            helpers.bulk(es_client, bulk)

    if isinstance(index_descriptions, list):
        for index_d in index_descriptions:
            index(index_d)
    else:
        index(index_descriptions)
